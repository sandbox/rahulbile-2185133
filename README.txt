Currently panels IPE do not allow editors to update the role visibility role to a panels pane through IPE. User needs to go backend of panelizer and Add / Update the visibility Rules. This module exposes a button in Panel IPE links area names 'RV' and allow end user to Add/update the existing/new visibility rule for Role plugin. Currently it will try to update any existing first access plugin of type Role or if does not exists, creates and add to the Pane access. Also currently supports with panes that are in the DB as opposed to in code.

Inspired from module Breakpoint panels
